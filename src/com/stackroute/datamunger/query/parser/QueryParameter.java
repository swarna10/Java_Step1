package com.stackroute.datamunger.query.parser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
/* 
 * This class will contain the elements of the parsed Query String such as conditions,
 * logical operators,aggregate functions, file name, fields group by fields, order by
 * fields, Query Type
 * */
public class QueryParameter {
	private String queryString;
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public String getFile() {
		queryString = queryString.toLowerCase();
		String[] arrSplit1 = queryString.split("\\bfrom\\b");
		String[] arrSplit2 = null;
		String fileName;
		if (arrSplit1[1].trim().contains("where")) {
			arrSplit2 = arrSplit1[1].split("\\bwhere\\b");
			fileName = arrSplit2[0].trim();
		} else {
			if (arrSplit1[1].trim().contains("group by")) {
				arrSplit2 = arrSplit1[1].split("\\bgroup by\\b");
				fileName = arrSplit2[0].trim();
			} else if (!arrSplit1[1].trim().contains("group by") && arrSplit1[1].trim().contains("order by")) {
				arrSplit2 = arrSplit1[1].split("\\border by\\b");
				fileName = arrSplit2[0].trim();
			} else {
				fileName = arrSplit1[1].trim();
			}
		}
//		System.out.println(fileName);
		return fileName;
	}
	public List<Restriction> getRestrictions() {
		// queryString = queryString.toLowerCase();
		List<Restriction> restrictionList = new ArrayList<Restriction>();
		String[] conditionsArr = null;
		if (queryString.contains("where") && !queryString.contains("group by") && !queryString.contains("order by")) {
			conditionsArr = null;
			conditionsArr = queryString.split("where")[1].trim().split(" and | or ");
			for (String arrVal : conditionsArr) {
				if (arrVal.contains(" > ") || arrVal.contains(" < ")) {
					Restriction restObj1 = new Restriction();
					String[] restrictionArr1 = arrVal.split(" > | < ");
					restObj1.setPropertyName(restrictionArr1[0].trim());
					restObj1.setPropertyValue(restrictionArr1[1].trim());
					String[] restrictionArr4 = arrVal.split("");
					for (String val : restrictionArr4) {
						if (val.equals(">") || val.equals("<")) {
							restObj1.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj1);
				}
				else if (arrVal.contains("=")) {
					Restriction restObj2 = new Restriction();
					String[] restrictionArr2 = arrVal.split("=");
					restObj2.setPropertyName(restrictionArr2[0].trim());
					restObj2.setPropertyValue(restrictionArr2[1].trim());
					String[] restrictionArr3 = arrVal.split("");
					for (String val : restrictionArr3) {
						if (val.equals("=")) {
							restObj2.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj2);
				}
			}
			return restrictionList;
		}
		if (queryString.contains("where") && queryString.contains("group by") && queryString.contains("order by")) {
			conditionsArr = null;
			conditionsArr = queryString.split("where")[1].trim().split("group by")[0].trim().split(" and | or ");
			for (String arrVal : conditionsArr) {
				if (arrVal.contains(" > ") || arrVal.contains(" < ")) {
					Restriction restObj1 = new Restriction();
					String[] restrictionArr1 = arrVal.split(" > | < ");
					restObj1.setPropertyName(restrictionArr1[0].trim());
					restObj1.setPropertyValue(restrictionArr1[1].trim());
					String[] restrictionArr4 = arrVal.split("");
					for (String val : restrictionArr4) {
						if (val.equals(">") || val.equals("<")) {
							restObj1.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj1);
				}
				else if (arrVal.contains("=")) {
					Restriction restObj2 = new Restriction();
					String[] restrictionArr2 = arrVal.split("=");
					restObj2.setPropertyName(restrictionArr2[0].trim());
					restObj2.setPropertyValue(restrictionArr2[1].trim());
					String[] restrictionArr3 = arrVal.split("");
					for (String val : restrictionArr3) {
						if (val.equals("=")) {
							restObj2.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj2);
				}
			}
			return restrictionList;
		}
		else if (queryString.contains("where") && !queryString.contains("group by")
				&& queryString.contains("order by")) {
			conditionsArr = null;
			conditionsArr = queryString.split("where")[1].trim().split("order by")[0].trim().split(" and | or ");
			// List<String> FindOperator = new ArrayList<String>();
			for (String arrVal : conditionsArr) {
				// out.println(arrVal);
				if (arrVal.contains(" > ") || arrVal.contains(" < ")) {
					Restriction restObj1 = new Restriction();
					String[] restrictionArr1 = arrVal.split(" > | < ");
					restObj1.setPropertyName(restrictionArr1[0].trim());
					restObj1.setPropertyValue(restrictionArr1[1].trim());
					String[] restrictionArr4 = arrVal.split("");
					for (String val : restrictionArr4) {
						if (val.equals(">") || val.equals("<")) {
							restObj1.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj1);
				}
				else if (arrVal.contains("=")) {
					Restriction restObj2 = new Restriction();
					String[] restrictionArr2 = arrVal.split("=");
					restObj2.setPropertyName(restrictionArr2[0].trim());
					restObj2.setPropertyValue(restrictionArr2[1].trim());
					String[] restrictionArr3 = arrVal.split("");
					for (String val : restrictionArr3) {
						if (val.equals("=")) {
							restObj2.setCondition(val.trim());
						}
					}
					restrictionList.add(restObj2);
				}
			}
			return restrictionList;
		}
		else {
			return restrictionList = null;
		}
	}
	public List<String> getLogicalOperators() {
		queryString = queryString.toLowerCase();
		String[] logicalOperators = null;
		List<String> conditionsList = new ArrayList<String>();
		if (queryString.contains("where")) {
			String[] conditionArray = queryString.split("\\bwhere\\b")[1].trim().split(" ");
			for (String element : conditionArray) {
				if (element.equalsIgnoreCase("and") || element.equalsIgnoreCase("or")) {
					conditionsList.add(element);
				}
			}
			logicalOperators = new String[conditionsList.size()];
			logicalOperators = conditionsList.toArray(logicalOperators);
			for (String operator : logicalOperators) {
//				System.out.println("LogicalOperator : " + operator);
			}
			return conditionsList;
		} else {
			return conditionsList = null;
		}
	}
	public List<String> getFields() {
		queryString = queryString.toLowerCase();
		List<String> fieldList = new ArrayList<String>();
		String[] selectFields = queryString.split("\\bfrom\\b")[0].trim().split("\\bselect\\b")[1].trim().split(",");
//		System.out.println("getFields: " + Arrays.toString(selectFields));
		if (!selectFields[0].contains("*")) {
			for (String val : selectFields) {
				fieldList.add(val.trim());
			}
			
			return fieldList;
		} else {
			return fieldList = null;
		}
	}
	public List<AggregateFunction> getAggregateFunctions() {
		queryString = queryString.toLowerCase();
		AggregateFunction aggregateFunc = new AggregateFunction();
		List<AggregateFunction> newField = new ArrayList<AggregateFunction>();
		String[] arraySpl1 = queryString.split("\\bselect\\b")[1].trim().split("\\bfrom\\b")[0].trim().split(",");
		List<String> aggrFuncList = new ArrayList();
		String aggField;
		String aggFunc;
		int i = 0;
		List<String> functions = new ArrayList<String>();
		List<String> fieldVal = new ArrayList<String>();
		for (String aggr : arraySpl1) {
			if (aggr.contains("min(") || aggr.contains("max(") || aggr.contains("avg(") || aggr.contains("sum(")
					|| aggr.contains("count(")) {
				String[] field = aggr.split("\\(");
				functions.add(field[0].trim());
				field = field[1].split("\\)");
				fieldVal.add(field[0].trim());
				aggregateFunc.setField(fieldVal.get(i));
				aggregateFunc.setFunction(functions.get(i));
				newField.add(aggregateFunc);
//				System.out.println(aggregateFunc.getFunction() + " - func");
//				System.out.println(aggregateFunc.getField() + " - field");
				i++;
			}
		}
//		System.out.println(Arrays.toString(functions.toArray()));
//		System.out.println(Arrays.toString(fieldVal.toArray()));
		if (!newField.isEmpty() || newField.size() != 0) {
			return newField;
		} else {
			return newField = null;
		}
	}
	public List<String> getGroupByFields() {
		queryString = queryString.toLowerCase();
		String[] groupByFieldsArr = null;
		List<String> grpByfieldList = new ArrayList<String>();
		if (queryString.contains("order by") && queryString.contains("group by")) {
			String[] arraySpl1 = queryString.split("\\bgroup by\\b")[1].trim().split("\\border by\\b")[0].trim()
					.split(",");
			groupByFieldsArr = new String[arraySpl1.length];
			for (int i = 0; i < arraySpl1.length; i++) {
				groupByFieldsArr[i] = arraySpl1[i].trim();
//				System.out.println("GroupBy Field : " + groupByFieldsArr[i]);
			}
			grpByfieldList = Arrays.asList(groupByFieldsArr);
			return grpByfieldList;
		} else if (!queryString.contains("order by") && queryString.contains("group by")) {
			String[] arraySpl1 = queryString.split("\\bgroup by\\b")[1].trim().split(",");
			groupByFieldsArr = new String[arraySpl1.length];
			for (int i = 0; i < arraySpl1.length; i++) {
				groupByFieldsArr[i] = arraySpl1[i].trim();
//				System.out.println("GroupBy Field : " + groupByFieldsArr[i]);
			}
			grpByfieldList = Arrays.asList(groupByFieldsArr);
			return grpByfieldList;
		} else {
			return grpByfieldList = null;
		}
	}
	public List<String> getOrderByFields() {
		queryString = queryString.toLowerCase();
		String[] orderByFieldsArr = null;
		List<String> ordByfieldList = new ArrayList<String>();
		if (queryString.contains("order by")) {
			String[] arraySpl1 = queryString.split("\\border by\\b")[1].trim().split(",");
			orderByFieldsArr = new String[arraySpl1.length];
			for (int i = 0; i < arraySpl1.length; i++) {
				orderByFieldsArr[i] = arraySpl1[i].trim();
//				System.out.println("OrderBy Field : " + orderByFieldsArr[i]);
			}
			ordByfieldList = Arrays.asList(orderByFieldsArr);
			return ordByfieldList;
		} else {
			return ordByfieldList = null;
		}
	}
}